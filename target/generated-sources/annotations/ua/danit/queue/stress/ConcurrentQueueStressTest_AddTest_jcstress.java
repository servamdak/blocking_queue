package ua.danit.queue.stress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import org.openjdk.jcstress.infra.runners.TestConfig;
import org.openjdk.jcstress.infra.collectors.TestResultCollector;
import org.openjdk.jcstress.infra.runners.Runner;
import org.openjdk.jcstress.infra.runners.StateHolder;
import org.openjdk.jcstress.util.Counter;
import org.openjdk.jcstress.vm.WhiteBoxSupport;
import java.util.concurrent.ExecutionException;
import ua.danit.queue.stress.ConcurrentQueueStressTest.AddTest;
import org.openjdk.jcstress.infra.results.LL_Result_jcstress;

public class ConcurrentQueueStressTest_AddTest_jcstress extends Runner<LL_Result_jcstress> {

    volatile StateHolder<AddTest, LL_Result_jcstress> version;

    public ConcurrentQueueStressTest_AddTest_jcstress(TestConfig config, TestResultCollector collector, ExecutorService pool) {
        super(config, collector, pool, "ua.danit.queue.stress.ConcurrentQueueStressTest.AddTest");
    }

    @Override
    public void sanityCheck() throws Throwable {
        sanityCheck_API();
        sanityCheck_Footprints();
    }

    private void sanityCheck_API() throws Throwable {
        final AddTest t = new AddTest();
        final AddTest s = new AddTest();
        final LL_Result_jcstress r = new LL_Result_jcstress();
        Collection<Future<?>> res = new ArrayList<>();
        res.add(pool.submit(() -> t.actor1()));
        res.add(pool.submit(() -> t.actor2()));
        for (Future<?> f : res) {
            try {
                f.get();
            } catch (ExecutionException e) {
                throw e.getCause();
            }
        }
        t.arbiter(r);
    }

    private void sanityCheck_Footprints() throws Throwable {
        config.adjustStrides(size -> {
            version = new StateHolder<>(new AddTest[size], new LL_Result_jcstress[size], 2, config.spinLoopStyle);
            final AddTest t = new AddTest();
            for (int c = 0; c < size; c++) {
                LL_Result_jcstress r = new LL_Result_jcstress();
                AddTest s = new AddTest();
                version.rs[c] = r;
                version.ss[c] = s;
                s.actor1();
                s.actor2();
            }
        });
    }

    @Override
    public Counter<LL_Result_jcstress> internalRun() {
        version = new StateHolder<>(new AddTest[0], new LL_Result_jcstress[0], 2, config.spinLoopStyle);

        control.isStopped = false;
        Collection<Future<Counter<LL_Result_jcstress>>> tasks = new ArrayList<>();
        tasks.add(pool.submit(this::actor1));
        tasks.add(pool.submit(this::actor2));

        try {
            TimeUnit.MILLISECONDS.sleep(config.time);
        } catch (InterruptedException e) {
        }

        control.isStopped = true;

        waitFor(tasks);

        Counter<LL_Result_jcstress> counter = new Counter<>();
        for (Future<Counter<LL_Result_jcstress>> f : tasks) {
            try {
                counter.merge(f.get());
            } catch (Throwable e) {
                throw new IllegalStateException(e);
            }
        }
        return counter;
    }

    public final void jcstress_consume(StateHolder<AddTest, LL_Result_jcstress> holder, Counter<LL_Result_jcstress> cnt, int a, int actors) {
        AddTest[] ss = holder.ss;
        LL_Result_jcstress[] rs = holder.rs;
        int len = ss.length;
        int left = a * len / actors;
        int right = (a + 1) * len / actors;
        for (int c = left; c < right; c++) {
            LL_Result_jcstress r = rs[c];
            AddTest s = ss[c];
            s.arbiter(r);
            ss[c] = new AddTest();
            cnt.record(r);
            r.r1 = null;
            r.r2 = null;
        }
    }

    public final void jcstress_updateHolder(StateHolder<AddTest, LL_Result_jcstress> holder) {
        if (!holder.tryStartUpdate()) return;
        AddTest[] ss = holder.ss;
        LL_Result_jcstress[] rs = holder.rs;
        int len = ss.length;

        int newLen = holder.updateStride ? Math.max(config.minStride, Math.min(len * 2, config.maxStride)) : len;

        AddTest[] newS = ss;
        LL_Result_jcstress[] newR = rs;
        if (newLen > len) {
            newS = Arrays.copyOf(ss, newLen);
            newR = Arrays.copyOf(rs, newLen);
            for (int c = len; c < newLen; c++) {
                newR[c] = new LL_Result_jcstress();
                newS[c] = new AddTest();
            }
         }

        version = new StateHolder<>(control.isStopped, newS, newR, 2, config.spinLoopStyle);
        holder.finishUpdate();
   }

    public final Counter<LL_Result_jcstress> actor1() {

        Counter<LL_Result_jcstress> counter = new Counter<>();
        while (true) {
            StateHolder<AddTest,LL_Result_jcstress> holder = version;
            if (holder.stopped) {
                return counter;
            }

            AddTest[] ss = holder.ss;
            LL_Result_jcstress[] rs = holder.rs;
            int size = ss.length;

            holder.preRun();

            for (int c = 0; c < size; c++) {
                AddTest s = ss[c];
                s.actor1();
            }

            holder.postRun();

            jcstress_consume(holder, counter, 0, 2);
            jcstress_updateHolder(holder);

            holder.postUpdate();
        }
    }

    public final Counter<LL_Result_jcstress> actor2() {

        Counter<LL_Result_jcstress> counter = new Counter<>();
        while (true) {
            StateHolder<AddTest,LL_Result_jcstress> holder = version;
            if (holder.stopped) {
                return counter;
            }

            AddTest[] ss = holder.ss;
            LL_Result_jcstress[] rs = holder.rs;
            int size = ss.length;

            holder.preRun();

            for (int c = 0; c < size; c++) {
                AddTest s = ss[c];
                s.actor2();
            }

            holder.postRun();

            jcstress_consume(holder, counter, 1, 2);
            jcstress_updateHolder(holder);

            holder.postUpdate();
        }
    }

}
