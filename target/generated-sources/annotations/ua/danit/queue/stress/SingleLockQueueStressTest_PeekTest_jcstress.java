package ua.danit.queue.stress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import org.openjdk.jcstress.infra.runners.TestConfig;
import org.openjdk.jcstress.infra.collectors.TestResultCollector;
import org.openjdk.jcstress.infra.runners.Runner;
import org.openjdk.jcstress.infra.runners.StateHolder;
import org.openjdk.jcstress.util.Counter;
import org.openjdk.jcstress.vm.WhiteBoxSupport;
import java.util.concurrent.ExecutionException;
import ua.danit.queue.stress.SingleLockQueueStressTest.PeekTest;
import org.openjdk.jcstress.infra.results.LL_Result_jcstress;

public class SingleLockQueueStressTest_PeekTest_jcstress extends Runner<LL_Result_jcstress> {

    volatile StateHolder<PeekTest, LL_Result_jcstress> version;

    public SingleLockQueueStressTest_PeekTest_jcstress(TestConfig config, TestResultCollector collector, ExecutorService pool) {
        super(config, collector, pool, "ua.danit.queue.stress.SingleLockQueueStressTest.PeekTest");
    }

    @Override
    public void sanityCheck() throws Throwable {
        sanityCheck_API();
        sanityCheck_Footprints();
    }

    private void sanityCheck_API() throws Throwable {
        final PeekTest t = new PeekTest();
        final PeekTest s = new PeekTest();
        final LL_Result_jcstress r = new LL_Result_jcstress();
        Collection<Future<?>> res = new ArrayList<>();
        res.add(pool.submit(() -> t.actor1(r)));
        res.add(pool.submit(() -> t.actor2(r)));
        for (Future<?> f : res) {
            try {
                f.get();
            } catch (ExecutionException e) {
                throw e.getCause();
            }
        }
    }

    private void sanityCheck_Footprints() throws Throwable {
        config.adjustStrides(size -> {
            version = new StateHolder<>(new PeekTest[size], new LL_Result_jcstress[size], 2, config.spinLoopStyle);
            final PeekTest t = new PeekTest();
            for (int c = 0; c < size; c++) {
                LL_Result_jcstress r = new LL_Result_jcstress();
                PeekTest s = new PeekTest();
                version.rs[c] = r;
                version.ss[c] = s;
                s.actor1(r);
                s.actor2(r);
            }
        });
    }

    @Override
    public Counter<LL_Result_jcstress> internalRun() {
        version = new StateHolder<>(new PeekTest[0], new LL_Result_jcstress[0], 2, config.spinLoopStyle);

        control.isStopped = false;
        Collection<Future<Counter<LL_Result_jcstress>>> tasks = new ArrayList<>();
        tasks.add(pool.submit(this::actor1));
        tasks.add(pool.submit(this::actor2));

        try {
            TimeUnit.MILLISECONDS.sleep(config.time);
        } catch (InterruptedException e) {
        }

        control.isStopped = true;

        waitFor(tasks);

        Counter<LL_Result_jcstress> counter = new Counter<>();
        for (Future<Counter<LL_Result_jcstress>> f : tasks) {
            try {
                counter.merge(f.get());
            } catch (Throwable e) {
                throw new IllegalStateException(e);
            }
        }
        return counter;
    }

    public final void jcstress_consume(StateHolder<PeekTest, LL_Result_jcstress> holder, Counter<LL_Result_jcstress> cnt, int a, int actors) {
        PeekTest[] ss = holder.ss;
        LL_Result_jcstress[] rs = holder.rs;
        int len = ss.length;
        int left = a * len / actors;
        int right = (a + 1) * len / actors;
        for (int c = left; c < right; c++) {
            LL_Result_jcstress r = rs[c];
            PeekTest s = ss[c];
            ss[c] = new PeekTest();
            cnt.record(r);
            r.r1 = null;
            r.r2 = null;
        }
    }

    public final void jcstress_updateHolder(StateHolder<PeekTest, LL_Result_jcstress> holder) {
        if (!holder.tryStartUpdate()) return;
        PeekTest[] ss = holder.ss;
        LL_Result_jcstress[] rs = holder.rs;
        int len = ss.length;

        int newLen = holder.updateStride ? Math.max(config.minStride, Math.min(len * 2, config.maxStride)) : len;

        PeekTest[] newS = ss;
        LL_Result_jcstress[] newR = rs;
        if (newLen > len) {
            newS = Arrays.copyOf(ss, newLen);
            newR = Arrays.copyOf(rs, newLen);
            for (int c = len; c < newLen; c++) {
                newR[c] = new LL_Result_jcstress();
                newS[c] = new PeekTest();
            }
         }

        version = new StateHolder<>(control.isStopped, newS, newR, 2, config.spinLoopStyle);
        holder.finishUpdate();
   }

    public final Counter<LL_Result_jcstress> actor1() {

        Counter<LL_Result_jcstress> counter = new Counter<>();
        while (true) {
            StateHolder<PeekTest,LL_Result_jcstress> holder = version;
            if (holder.stopped) {
                return counter;
            }

            PeekTest[] ss = holder.ss;
            LL_Result_jcstress[] rs = holder.rs;
            int size = ss.length;

            holder.preRun();

            for (int c = 0; c < size; c++) {
                PeekTest s = ss[c];
                LL_Result_jcstress r = rs[c];
                r.trap = 0;
                s.actor1(r);
            }

            holder.postRun();

            jcstress_consume(holder, counter, 0, 2);
            jcstress_updateHolder(holder);

            holder.postUpdate();
        }
    }

    public final Counter<LL_Result_jcstress> actor2() {

        Counter<LL_Result_jcstress> counter = new Counter<>();
        while (true) {
            StateHolder<PeekTest,LL_Result_jcstress> holder = version;
            if (holder.stopped) {
                return counter;
            }

            PeekTest[] ss = holder.ss;
            LL_Result_jcstress[] rs = holder.rs;
            int size = ss.length;

            holder.preRun();

            for (int c = 0; c < size; c++) {
                PeekTest s = ss[c];
                LL_Result_jcstress r = rs[c];
                r.trap = 0;
                s.actor2(r);
            }

            holder.postRun();

            jcstress_consume(holder, counter, 1, 2);
            jcstress_updateHolder(holder);

            holder.postUpdate();
        }
    }

}
