package ua.danit.queue.impl;

import java.util.AbstractQueue;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class LockQueue<T> extends AbstractQueue<T> {

    private Queue<T> queue;
    private Lock lock;

    public LockQueue () {
        queue = new LinkedList<>();
        lock = new ReentrantLock();
    }

    @Override
    public Iterator<T> iterator() {
        lock.lock();
        try {
            return new LinkedList<>(queue).iterator();
        } finally {
            lock.unlock();
        }
    }

    @Override
    public int size() {
        lock.lock();
        return 0;
    }

    @Override
    public boolean offer(T t) {
        lock.lock();
        try {
            return offer(t);
        } finally {
            lock.unlock();
        }

    }

    @Override
    public T poll() {
        return null;
    }

    @Override
    public T peek() {
        return null;
    }
}
