package ua.danit.queue.Semaphore;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

public class SemaphoreMain {
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(10);

        Semaphore semaphore = new Semaphore(5);


        Runnable runnable = () -> {
            boolean acquired = false;

            try {

                 acquired = semaphore.tryAcquire(1, 1000, TimeUnit.MILLISECONDS);
//                 acquired = semaphore.tryAcquire(1);

                if (acquired) {
                    System.out.println(Thread.currentThread().getName() + ": Semaphore acquired");
                } else {
                    System.out.println(Thread.currentThread().getName() + ": Could not acquired semaphore ");
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException io){

                }

            } catch (Exception ie) {
            } finally {
                if (acquired) {
                    System.out.print(Thread.currentThread().getName() + ": Semaphore release \r\n");
                    semaphore.release();
                }
            }
        };

        IntStream.range(0, 10).forEach(i ->  executorService.submit(runnable));

        try {
            Thread.sleep(20000);
        } catch (InterruptedException io){

        }

        System.exit(0);

    }
}
