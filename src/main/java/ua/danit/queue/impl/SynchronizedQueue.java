package ua.danit.queue.impl;

import java.util.*;

public class SynchronizedQueue<T> extends AbstractQueue<T> {

    private Queue<T> queue;
    private Object monitor;

    public SynchronizedQueue() {
        queue = new LinkedList<>();
        monitor = new Object();
    }

    @Override
    public Iterator<T> iterator() {
        synchronized (monitor) {
            return queue.iterator();
        }
    }

    @Override
    public int size() {
        synchronized (monitor) {
            return queue.size();
        }
    }

    @Override
    public boolean offer(T t) {
        synchronized (monitor) {
            return queue.offer(t);
        }
    }

    @Override
    public T poll() {
        synchronized (monitor) {
            return queue.poll();
        }
    }

    @Override
    public T peek() {
        synchronized (monitor) {
            return queue.peek();
        }
    }
}
